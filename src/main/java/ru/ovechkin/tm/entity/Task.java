package ru.ovechkin.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Getter
@Setter
public class Task extends AbstractEntity implements Serializable {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private String userId;

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}