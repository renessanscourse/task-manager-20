package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public class AbstractRepository<E extends AbstractEntity> {
    @NotNull
    protected final List<E> entities = new ArrayList<>();

    @NotNull
    public List<E> findAll() {
        return entities;
    }

    @NotNull
    public E merge(@NotNull final E e) {
        entities.add(e);
        return e;
    }

    public void merge(@NotNull final List<E> es) {
        for (@NotNull final E e: es) merge(e);
    }

    public void merge(@NotNull final E... es) {
        for (@NotNull final E e: es) merge(e);
    }

    public void clear() {
        entities.clear();
    }

    public void load(@NotNull final List<E> e) {
        clear();
        merge(e);
    }

    public void load(@NotNull final E... e) {
        clear();
        merge(e);
    }

}
