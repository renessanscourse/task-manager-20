package ru.ovechkin.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.exeption.user.AlreadyLoggedInException;
import ru.ovechkin.tm.util.TerminalUtil;

public class LoginCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.LOGIN;
    }

    @NotNull
    @Override
    public String description() {
        return "Login in your account";
    }

    @Override
    public void execute() {
        if (serviceLocator.getAuthService().isAuth()) throw new AlreadyLoggedInException();
        System.out.println("[LOGIN]");
        System.out.print("ENTER LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @Nullable final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
        System.out.println("[OK]");
    }

}