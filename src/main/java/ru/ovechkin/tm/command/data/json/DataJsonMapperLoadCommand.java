package ru.ovechkin.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.constant.PathToSavedFile;
import ru.ovechkin.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;

public class DataJsonMapperLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.DATA_JSON_MAPPER_LOAD;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from json file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON LOAD]");
        @Nullable final File file = new File(PathToSavedFile.DATA_JSON_MAPPER);
        @Nullable final FileInputStream fileInputStream = new FileInputStream("./dataMapper.json");
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final Domain domain = objectMapper.readValue(file, Domain.class);

        serviceLocator.getDomainService().load(domain);
        fileInputStream.close();
        System.out.println("[OK]");
    }

}