package ru.ovechkin.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.constant.PathToSavedFile;
import ru.ovechkin.tm.dto.Domain;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DataBinaryLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.DATA_BIN_LOAD;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from binary file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY LOAD]");
        @Nullable final FileInputStream fileInputStream = new FileInputStream(PathToSavedFile.DATA_BIN);
        @Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @Nullable final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getDomainService().load(domain);
        objectInputStream.close();
        fileInputStream.close();
        System.out.println("[OK]");
    }

}