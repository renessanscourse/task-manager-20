package ru.ovechkin.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.constant.PathToSavedFile;
import ru.ovechkin.tm.dto.Domain;

import java.io.*;
import java.nio.file.Files;

public class DataBinarySaveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.DATA_BIN_SAVE;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to binary file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY SAVE]");

        @Nullable final Domain domain = new Domain();
        serviceLocator.getDomainService().save(domain);

        @Nullable final File file = new File(PathToSavedFile.DATA_BIN);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @Nullable final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();

        System.out.println("[OK]");
        System.out.println();
    }

}