package ru.ovechkin.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.constant.PathToSavedFile;
import ru.ovechkin.tm.dto.Domain;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

public class DataBase64SaveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.DATA_BASE64_SAVE;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to base64 file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 SAVE]");

        @Nullable final Domain domain = new Domain();
        serviceLocator.getDomainService().save(domain);

        @Nullable final File file = new File(PathToSavedFile.DATA_BASE64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @Nullable final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();

        @Nullable final byte[] bytes = byteArrayOutputStream.toByteArray();
        @Nullable final String base64 = new BASE64Encoder().encode(bytes);
        byteArrayOutputStream.close();

        @Nullable final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.close();

        System.out.println("[OK]");
        System.out.println();
    }

}