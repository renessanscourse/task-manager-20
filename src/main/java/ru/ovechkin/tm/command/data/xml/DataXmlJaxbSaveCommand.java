package ru.ovechkin.tm.command.data.xml;

import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.constant.PathToSavedFile;
import ru.ovechkin.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.nio.file.Files;

public class DataXmlJaxbSaveCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.DATA_XML_JAXB_SAVE;
    }

    @Override
    public String description() {
        return "Save data to xml file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML SAVE]");

        @Nullable final Domain domain = new Domain();
        serviceLocator.getDomainService().save(domain);

        @Nullable final File file = new File(PathToSavedFile.DATA_XML_JAXB);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @Nullable final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(domain, file);

        System.out.println("[OK]");
    }

}