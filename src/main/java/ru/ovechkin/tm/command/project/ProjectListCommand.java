package ru.ovechkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.entity.Project;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_PROJECT_LIST;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[LIST PROJECT]");
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findUserProjects(userId);
        @NotNull int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
    }

}