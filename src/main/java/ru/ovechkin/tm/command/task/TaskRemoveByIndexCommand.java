package ru.ovechkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.TASK_REMOVE_BY_INDEX;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by index";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE TASK]");
        System.out.print("ENTER TASK INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Task task = serviceLocator.getTaskService().removeTaskByIndex(userId, index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

}