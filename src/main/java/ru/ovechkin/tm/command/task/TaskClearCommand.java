package ru.ovechkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.util.TerminalUtil;

public final class TaskClearCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_TASK_CLEAR;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getTaskService().removeAllTasks(userId);
        System.out.println("[OK]");
    }

}