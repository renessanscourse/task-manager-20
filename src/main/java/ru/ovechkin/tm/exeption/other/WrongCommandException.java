package ru.ovechkin.tm.exeption.other;

public class WrongCommandException extends RuntimeException {

    public WrongCommandException(final String command) {
        super("Error! Command `"+ command +"` does not exist...");
    }

}