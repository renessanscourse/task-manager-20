package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService extends IService<Project> {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable  String name, @Nullable String description);

    void add(@Nullable String userId, @Nullable Project project);

    @Nullable
    List<Project> findUserProjects(@Nullable String userId);

    void removeAllProjects(@Nullable String userId);

    @Nullable
    Project findProjectById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project findProjectByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Project findProjectByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Project updateProjectById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @Nullable
    Project updateProjectByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @Nullable
    Project removeProjectById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project removeProjectByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Project removeProjectByName(@Nullable String userId, @Nullable String name);

    void clear();

}