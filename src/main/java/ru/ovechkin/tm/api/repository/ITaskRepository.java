package ru.ovechkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    void add(@NotNull String userId, @NotNull Task task);

    void remove(@NotNull String userId, @NotNull Task task);

    @NotNull
    List<Task> findAllUserTask(@NotNull String userId);

    Task findById(@NotNull String userId, @NotNull String id);

    Task findByIndex(@NotNull String userId, @NotNull Integer index);

    Task findByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task removeById(@NotNull String userId, @NotNull String id);

    Task removeByIndex(@NotNull String userId, @NotNull Integer index);

    Task removeByName(@NotNull String userId, @NotNull String name);

}