package ru.ovechkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.entity.User;

import java.util.Collection;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User add(@NotNull User user);

    User findById(@NotNull String id);

    User findByLogin(@NotNull String login);

    @NotNull
    User removeUser(@NotNull User user);

    User removeById(@NotNull String id);

    User removeByLogin(@NotNull String login);

}