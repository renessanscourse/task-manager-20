package ru.ovechkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository <E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    @NotNull
    E merge(@NotNull E e);

    void merge(@NotNull List<E> e);

    void merge(@NotNull E... e);

    void clear(@NotNull String userId);

    void load(@NotNull List<E> e);

    void load(@NotNull E... e);

    void clear();

}
