package ru.ovechkin.tm.api.repository;

import java.util.Collection;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Task;

public interface IProjectRepository extends IRepository<Project> {

    void add(@NotNull String userId, @NotNull Project project);

    void remove(@NotNull String userId, @NotNull Project project);

    @NotNull
    List<Project> findUserProjects(@NotNull String userId);

    @NotNull
    Project findById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project findByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project findByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project removeById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project removeByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project removeByName(@NotNull String userId, @NotNull String name);

}